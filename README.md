# Websites acad�micos

Los websites que se encuentran en este repositorio atienden a proyectos o pr�cticas realizadas a lo largo de una formaci�n acad�mica y/o con prop�sito de aprendizaje de diversas tecnolog�as webs.

## Proyecto prototipo "El Forastero" desarrollado junto a ESDI

El proyecto acad�mico ten�a por objetivo realizar un prototipo de web colaborando con dise�adores de la Escuela Superior de Dise�o Ram�n Llull.

Se estableci� un grupo de 3 programadores del CFGS/DAM de IES Sabadell y 2 dise�adoras de la ESDI.

Se realizaron todas las etapas iniciales de dise�o como la tem�tica, tipo contenido, el wireframe y gr�ficos por parte de las dise�adoras.
Los programadores realizaron un trabajo de abstracci�n para trasladar dichas ideas y dise�os en un prototipo web que sirviera para una demostraci�n durante la presentaci�n en p�blico en la ESDI.

### Instrucciones de instalaci�n

/esdi6

Contiene todo el mapa web desarrollado, copiar a la carpeta del localhost donde se desea comprobar la funcionalidad.  Ejecutar solamente en un entorno controlado para pruebas y estudio.

/esdi6/esdi11.sql

Ejecutar el archivo "esdi11.sql" en el gestor MySQL para cargar la BBDD ("esdi").

/materialsesdi

El material que las dise�adoras nos entregaron a los programadores.

Configuraci�n:
Editar el archivo "/esdi6/utilidades/connection.php" y ajustar las variables $db_host, $db_usuari y $db_contrasenya de acuerdo a las necesidades de entorno para poder conectar con la BBDD.

### Tecnolog�as usadas

* PHP5
* MySQL
* MySQL Workbench
* Notepad++
* HTML5 + CSS3
* Javascript + AJAX
* GIMP
* Shotcut

### Aviso y uso

Tiene algunos usuarios preparados para usar, por ejemplo: "pepe" con pass "hola"; se puede realizar el registro de usuarios, pero el campo de formulario "alergia" debe ser seleccionado como "marisco" o no escoger nada para surtir efecto ya que no se han introducido suficientes recetas para realizar m�s pruebas.
Al escogerse marisco, aquellas recetas que lo contienen aparecer�n ensombrecidas de gris para avisar al usuario de posibles recetas que no le puedan interesar.

Algunas funcionalidades que combinan AJAX con PHP+MySQL son:
* Se puede generar una lista de favoritos con darle al coraz�n, tipo "me gusta" de otras redes.
* Se pueden escoger puntuaciones con las estrellas y la media se calcula en funci�n de los votos realizados.
* Tiene un buscador arriba a la derecha que genera consultas inmediatas de coincidencias para las recetas y restaurantes con su autocompletado.

Para la demostraci�n solamente se habilit� por cuestiones de cantidad de datos para la BBDD:
* Al buscar recetas, la categor�a ASIA > Jap�n > Sushi
* Al buscar restaurantes, ASIA > Udon y ASIA > Kaitensushi

Por falta de tiempo para entregar y exponer, no se pudo mejorar aspectos como la usabilidad, la accesibilidad y un dise�o 100% responsive.

Ten en cuenta que forma parte de una pr�ctica acad�mica y por tanto no deber�a usarse en entorno de producci�n, por incompleto e inseguro. Existe mucho c�digo basura comentado que proviene de otro proyecto previo, puede ignorarse o borrarse.

No me responsabilizo de los da�os que puedan derivarse del mal uso intencionado o no.