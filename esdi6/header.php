
<?php
	
	session_start();
	include('utilidades/connection.php');
	if (!isset($_SESSION["user"])){
		// REGISTRO DE USUARIO
		if (isset($_POST["nombre"]) and isset($_POST["apellido"]) and isset($_POST["nick"]) and isset($_POST["pass"]) and isset($_POST["edad"]) and isset($_POST["pais"]) and isset($_POST["alergias"])){
				$nom = $_POST["nombre"];
				$apellido = $_POST["apellido"];
				$nick = $_POST["nick"];
				$pass = $_POST["pass"];
				$pass = hash("sha512",$pass,false);
				$edad = $_POST["edad"];
				$pais = 1;
				$ciudad = 4;
				$alergias = $_POST["alergias"];


				$sql = "select max(id) as id from usuarios ";
				// FALTA CAMBIAR LAS TABLAS PARA PONER LAS ALERGIAS



						$resultat=mysqli_query ($connexio, $sql);
						if (!$resultat) die ("ERROR: No se pudo ejecutar consulta SQL para crear una ID");


						$fila=mysqli_fetch_array ($resultat);

						$id = $fila['id'] + (1);


						$insert=sprintf("INSERT INTO usuarios VALUES ('%d','%s','%s','%s','%s','%d','%d','%d')  ",mysqli_real_escape_string($connexio,$id),mysqli_real_escape_string($connexio, $nom),mysqli_real_escape_string($connexio,$apellido),mysqli_real_escape_string($connexio,$nick),mysqli_real_escape_string($connexio,$pass),mysqli_real_escape_string($connexio,$edad),mysqli_real_escape_string($connexio,$pais),mysqli_real_escape_string($connexio,$ciudad));
						$resultat=mysqli_query($connexio,$insert);
						if(!$resultat){
								die ("No se pudo ejecutar la consulta SQL para insertar usuario");
						}
						else{

							$insert=sprintf("INSERT INTO usuarios_has_alergias (alergias_id, usuarios_id) VALUES ('%d','%d')  ",mysqli_real_escape_string($connexio,$alergias),mysqli_real_escape_string($connexio, $id));
							$resultat=mysqli_query($connexio,$insert);
							if(!$resultat){
									die ("No se pudo ejecutar la consulta SQL para introducir alergia.");
							}else{
								$_SESSION["user"]=$nick;
								$_SESSION["id"]=$id;
							}
						}
					//	"<script>alert('Registrat correctament'); window.location = 'index.php';</script>";

						//mysqli_close($connexio);
		}
		//  LOGIN COMPRUEVA USUARIO Y PASS EXISTEN
		if(isset($_POST["userLogin"]) && isset($_POST["passLogin"])){

			$sql = "select max(id) as id from usuarios ";
			$usuari = $_POST["userLogin"];
			$password = $_POST["passLogin"];
			$password = hash("sha512",$password,false);


			$consulta=sprintf("SELECT * FROM usuarios WHERE alias='%s' AND password='%s'",mysqli_real_escape_string($connexio, $usuari),mysqli_real_escape_string($connexio, $password));
				$consultaNom=sprintf("SELECT * FROM usuarios WHERE alias='%s'",mysqli_real_escape_string($connexio, $usuari));
				$resultat=mysqli_query($connexio,$consulta);
				$resultatNom=mysqli_query($connexio, $consultaNom);

				if(!$resultat || !$resultatNom){
					die ("No se pudo ejecutar la consulta SQL");
				}

				$login = false;
				$userOK = false;

				if (mysqli_num_rows($resultatNom) > 0){
					$userOK = true;
				}

				if (mysqli_num_rows($resultat) > 0){
					$login = true;
					$fila = mysqli_fetch_assoc($resultat);
					$id = $fila['id'];
				}


				if ($login){
					$_SESSION["user"] = $usuari;
					$_SESSION["id"] = $id;
					$_SESSION["userLogin"]=null;
					$_SESSION["passLogin"]=null;
				}
				//mysqli_close($connexio);
		}

	}


?>
<!DOCTYPE html>
<html lang="es" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>El Forastero</title>
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/fondo.css" />
	<link rel="stylesheet" type="text/css" href="css/video.css" />
	<link rel="stylesheet" type="text/css" href="css/modal.css" />
	<!--<link rel="stylesheet" href="CSS/exotica.css">-->
	<!--<link rel="stylesheet" href="CSS/mapa.css"> <!--Estilo para el mapa - Jordi-->
	<link rel="stylesheet" href="css/search.css"> <!--Estilo para el buscador - Jordi-->
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/icons.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" type="text/css" href="css/pestanas.css" />
	<link rel="stylesheet" type="text/css" href="css/header.css" />
	<link rel="stylesheet" type="text/css" href="css/favoritos.css" />
	<link rel="stylesheet" type="text/css" href="css/restaurantes.css" />
	<link rel="stylesheet" type="text/css" href="css/sakura.css" />
	<link rel="stylesheet" type="text/css" href="css/logout.css" />
	<link rel="stylesheet" type="text/css" href="css/recetas.css" />
	<link rel="stylesheet" type="text/css" href="css/heart.css" />
	<!--<link rel="stylesheet" type="text/css" href="css/csslogo.css" />-->
	

	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/modaljs.js"></script>
	<script src="js/modernizr.custom.js"></script>
	<script src="js/viewportSize.js"></script>
	<script src="js/video.js"></script>
	<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>




	<script>
	function cambiarSidebar(opcion){

		if (opcion == 1){
				$("#div_m11").css("display","block");
				$("#div_m12").css("display","none");
		}
		else{
			$("#div_m12").css("display","block");
			$("#div_m11").css("display","none");
		}

	}

	</script>
</head>
<body>
	<div id="st-container" class="st-container">
			<!--
				example menus
				these menus will be on top of the push wrapper
			-->


			<nav class="st-menu st-effect-11" id="menu-11">
				<div id="div_m11" style="display:none;">
				<h2>CONTINENTES</h2>
				<ul>

					<li><a><img class="icon-continent"src="images/Asia-icon.png"><span>ASIA</span></a>
						<div class="liniaTitulos"></div>
						<ul>
							<h2 >PAÍSES</h2>
							<li>Japón <div class="linia"></div>
									<ul>
										<h2>PLATOS </h2>
										<li style="cursor:pointer;" onclick="window.location.href = 'sushi.php?search=1';">Sushi <div class="linia"></div> </li>
										<li>Tempura <div class="linia"></div> </li>
										<li>Sopas <div class="linia"></div> </li>
										<li>Pescados <div class="linia"></div> </li>
										<li>Arroces <div class="linia"></div></li>
										<li>Fideos <div class="linia"></div></li>
									</ul>
							</li>
							<li>China <div class="linia"></div></li>
							<li>Tailandia<div class="linia"></div></li>
							<li>Vietnam<div class="linia"></div></li>
							<li>Nepal<div class="linia"></div></li>
							<li>India<div class="linia"></div></li>
						</ul>
					</li>
					<li><a><img class="icon-continent"src="images/europe-icon.png"><span>EUROPA</span></a>	<div class="liniaTitulos"></div></li>
					<li><a><img class="icon-continent"src="images/america-icon.png"><span>AMÉRICA N.</span></a>	<div class="liniaTitulos"></div></li>
					<li><a><img class="icon-continent"src="images/south-america-icon.png"><span>AMÉRICA S.</span></a>	<div class="liniaTitulos"></div></li>
					<li><a><img class="icon-continent"src="images/australia-icon.png"><span>OCEANÍA</span></a>	<div class="liniaTitulos"></div></li>
					<li><a><img class="icon-continent"src="images/africa-icon.png"><span>ÁFRICA</span></a>	<div class="liniaTitulos"></div></li>
				</ul>
			</div>

			<div  id="div_m12" style="display:none;">
			<h2>CONTINENTES</h2>
			<ul>

				<li><a><img class="icon-continent"src="images/Asia-icon.png"><span>ASIA</span></a>
					<div class="liniaTitulos"></div>
					<ul>
						<h2 >BUSCA</h2>
						<li onclick="window.location='restaurantes.php?id=2'" style="cursor:pointer;">Udon <div class="linia"></div></li>
						<li onclick="window.location='restaurantes.php?id=1'" style="cursor:pointer;">Kaitensushi <div class="linia"></div></li>

					</ul>
				</li>
				<li><a><img class="icon-continent"src="images/europe-icon.png"><span>EUROPA</span></a>	<div class="liniaTitulos"></div></li>
				<li><a><img class="icon-continent"src="images/america-icon.png"><span>AMÉRICA N.</span></a>	<div class="liniaTitulos"></div></li>
				<li><a><img class="icon-continent"src="images/south-america-icon.png"><span>AMÉRICA S.</span></a>	<div class="liniaTitulos"></div></li>
				<li><a><img class="icon-continent"src="images/australia-icon.png"><span>OCEANÍA</span></a>	<div class="liniaTitulos"></div></li>
				<li><a><img class="icon-continent"src="images/africa-icon.png"><span>ÁFRICA</span></a>	<div class="liniaTitulos"></div></li>
			</ul>
		</div>

			</nav>

			<!-- content push wrapper -->
			<div class="st-pusher">
				<!--
					example menus
					these menus will be under the push wrapper
				-->

				<div class="st-content"><!-- this is the wrapper for the content -->
					<div class="st-content-inner"><!-- extra div for emulating position:fixed of the menu -->
						<header>
							<a href="index.php"><div class="logo"></div></a>

							<div class="menu">
								<div id="st-trigger-effects" >
									<button id="btn_recetas_menu" onclick="cambiarSidebar(1)" data-effect="st-effect-11" class="menu-opc">Recetas</button>
								</div>
								<div id="st-trigger-effects" >
									<button id="btn_restaurantes_menu" onclick="cambiarSidebar(2)" data-effect="st-effect-11" class="menu-opc">Restaurantes</button>
								</div>
								<?php

								if (isset($_SESSION["user"])){
									/*echo '<div id="st-trigger-effects" >
										<button data-effect="st-effect-11">Favoritos</button>
										</div>';*/
									echo '<div id="st-trigger-effects" >
											<a href="favoritos.php" class="menu-opc"  id="favoritos">Favoritos</a>
										 </div>';
										 //
										 //<button class="menu-opc">Favoritos</button>
								}


								?>
							</div>

							<div class="login">
								<div id="avatar_buscar">

									<div class="buscar">
										<form action="search-process.php" method="get">
											<!--<button class="lupa-search"> </button>-->
										<!--<input class="lupa" type="image" src="images/lupa-01.png" alt="Buscar" />-->
										<!-- Para cargar el buscador -->
										<?php
										include('search.php');
										?>
										</form>

									</div>
									<?php
									if(isset($_SESSION["user"])){
										echo '<div class="avatar"></div>';
									}else{
										echo '<div class="avatar"></div>';
									}

									?>
								</div>
								<?php
									if(isset($_SESSION["user"])){
										echo '<span>'.$_SESSION["user"].'</span>';
										echo '<span><a href="logo2.php" class="menu-opc">Salir</a>
										
										</span>';
										
										//<form action="login-out.php" method="post">
										//<input type="hidden" name="nicklogout" value="'.$_SESSION["user"].'">
										//<button class="menu-opc">Salir</button>
										//</form>
									}else{
										echo "<div id='inicio_registro'>";
										echo "	<span onclick='modal(1);' class='menu-opc'>Inicio</span>";
										echo "  <span onclick='modal(2);' class='menu-opc'>Registro</span>";
										echo "</div>";

									}
								?>

							</div>
							<div class="liniaHeader"></div>
						</header>

						<div class="main clearfix">
