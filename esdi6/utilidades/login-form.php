<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<title>Utilidad para loguear</title>
<meta charset="UTF-8" />
		<meta name="description" content="Exercici 3 PHP M7 2n DAM" />
		<meta name="keywords" content="html, css, php" />
		<meta name="author" content="Jordi Palomar" />
		
		<link rel="stylesheet" type="text/css" href="./css/utilidades.css">
</head>
<body>
<div class="center"><h1>Utilidad para loguearse rápidamente</h1></div>
<div class="menu">
	
	
	
</div>
<?php
if (!isset($_SESSION["usuario"])){
	//Si no existeix cap sessió llavors mostrem formulari per loguejar-se
	echo '<form action="login-backend.php" method="post" class="form-style-9">
	<input type="text" name="usuari" class="field-style field-full align-none user" placeholder="Usuario">
	<input type="password" name="password" class="field-style field-full align-none pass" placeholder="Contraseña">
	<div class="right"> <a href="#">Recuperar password</a></div>
	<br><br>
	<div class="center">
	<input type="submit" value="Login">
	</div>
	</form>';
}else{
	echo "<div class='center login'>¡Bienvenid@ ".$_SESSION["usuario"]."!";
	echo "<br><img src='img/nepnep.gif' width='20%'><br><span style='color:#d298d9'>Nep-nep!!</span></div>";
}
?>
</body>
</html>


