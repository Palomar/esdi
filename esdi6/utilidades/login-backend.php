<?php
//Dades globals de connexió
include('connection.php');

//Declaració array

session_start();

if (!isset($_SESSION["usuario"])){
//Comprobació del METHOD
if ($_SERVER["REQUEST_METHOD"] == "POST") {
if (isset($_POST["usuari"])){
		if (isset($_POST["password"])){
			//Si trobem usuari i pass processa
			$usuari = $_POST["usuari"];
			$password = $_POST["password"];
			$password = hash("sha512",$password,false);
			
			//Consulta
			$consulta=sprintf("SELECT * FROM usuarios WHERE alias='%s' AND password='%s'",mysqli_real_escape_string($connexio, $usuari),mysqli_real_escape_string($connexio, $password));
			$consultaNom=sprintf("SELECT * FROM usuarios WHERE alias='%s'",mysqli_real_escape_string($connexio, $usuari));
			$resultat=mysqli_query($connexio,$consulta);
			$resultatNom=mysqli_query($connexio, $consultaNom);
			
			if(!$resultat || !$resultatNom){
				die ("No se pudo ejecutar la consulta SQL");
			}
			
			$login = false;
			$userOK = false;
			
			if (mysqli_num_rows($resultatNom) > 0){
				$userOK = true;
			}
			
			if (mysqli_num_rows($resultat) > 0){
				$login = true;
				$fila = mysqli_fetch_assoc($resultat);
				$id = $fila['id'];
			}
			
			
			if ($login){
				$_SESSION["usuario"] = $usuari;
				$_SESSION["id"] = $id;
			}
			mysqli_close($connexio);
			header("Refresh: 1; url=../index.php");
		}else header("Refresh: 1; url=../index.php");
    }else header("Refresh: 1; url=../index.php");
}else header("Refresh: 1; url=../index.php");
}
?>
<!DOCTYPE html>
<html lang="es"> 
<head> 
    <meta charset="UTF-8" />
	<title>Logueo en proceso</title>
	<head>
	<meta name="description" content="Exercici 4 PHP M7 2n DAM" />
		<meta name="keywords" content="html, css, php" />
		<meta name="author" content="Jordi Palomar" />
		<link rel="stylesheet" type="text/css" href="./css/utilidades.css">
	
	</head>
</head>
<body> 
<?php

/* Mostra missatge benvinguda
*/
if (isset($_SESSION["usuario"])){
	echo "<div class='center centerlog'>Te has logueado correctamente ".$_SESSION["usuario"]."</div>";
	
	
}else {
	if (isset($userOK)){
		echo "<div class='center centerlog'>No te has logueado correctamente.".(($userOK)? " El password es incorrecto.":" El usuario es incorrecto.")."</div>";
		if(!$userOK){
			//Incrustar el formulario de registro
			//include '';
		}else {
			//Postureo, no hay nada funcional en esto
			echo "<div class='center'><h3> <a href='#'>Recuperar password</a></h3></div>";
		}
	}else{
	//Cas que s'accedeixi sense fer POST del formulari
	echo "<span class='logout'>Acceso denegado</span>";
	}
} 

?>
</body> 
</html>