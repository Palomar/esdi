<!DOCTYPE html>
<html>
<head>
<title>Utilidad conexión BBDD</title>
<meta charset="UTF-8" />
		<meta name="description" content="Prueba de conexión BBDD" />
		<meta name="keywords" content="esdi" />
		<meta name="author" content="Jordi Palomar" />
</head>
<body>
<?php 

// Función encargada de formatear campos de una consulta combinada,
// nos ahorrará trabajo al no tener que estar usando alias en las consultas
function mysqli_fetch_array_esdi($rs) {
	$array_formateada = array();
	for ($i = 0; $i < mysqli_num_fields($rs); ++$i) {
		$info = mysqli_fetch_field_direct($rs, $i);
		array_push($array_formateada, "$info->table.$info->name");
		//echo $qualified_names[$i]." ";
	}
  
	if ($fila = mysqli_fetch_row($rs)) {
		$fila = array_combine($array_formateada, $fila);      
	}
	return $fila;
}

echo "Prueba de conexión ";

$db_host="localhost";
$db_nomBBDD="esdi";
$db_usuari="root";
$db_contrasenya="";
$connexio="";
$connexio= mysqli_connect ($db_host,$db_usuari,$db_contrasenya,$db_nomBBDD);
if (mysqli_connect_errno()) {
		echo "No se pudo conectar a la BBDD: " . mysqli_connect_errno();
		exit ();
 }
mysqli_set_charset($connexio, "utf8");
$consulta="SELECT * FROM usuarios JOIN paises ON usuarios.paises_id = paises.id JOIN ciudades ON usuarios.ciudades_id = ciudades.id";
$rs=mysqli_query($connexio,$consulta);
if(!$rs){
	die ("No se pudo ejecutar la consulta SQL");
}
//Ejemplo para mostrar campos de varias tablas combinadas
while ($fila=mysqli_fetch_array_esdi($rs)){
		echo "<br>";
		echo $fila['usuarios.id'];
		echo " ".$fila['usuarios.nombre'];
		echo " ".$fila['usuarios.apellidos'];
		echo " ".$fila['usuarios.alias'];
		echo " ".$fila['usuarios.password'];
		echo " ".$fila['usuarios.edad'];
		echo " ".$fila['paises.nombre'];
		echo " ".$fila['ciudades.nombre'];
	}
mysqli_close($connexio);
?>

</body>
</html>