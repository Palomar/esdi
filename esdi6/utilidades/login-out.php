<?php
session_start();
header("Refresh: 3; url=../index.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Utilidad de logout</title>
<meta charset="UTF-8" />
		<meta name="description" content="Exercici 5 PHP M7 2n DAM" />
		<meta name="keywords" content="html, css, php" />
		<meta name="author" content="Jordi Palomar" />
		
		<link rel="stylesheet" type="text/css" href="./css/utilidades.css">
</head>
<body>
<div class="center"><h1>Utilidad para desloguearse rápidamente</h1></div>
<?php
if(isset($_SESSION["usuario"])) {
	echo "<span class='logout'>Hasta la vista ".$_SESSION["usuario"]."</span>";
	session_destroy();
}
?>
</body>
</html>


