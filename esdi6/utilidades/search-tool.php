<!DOCTYPE html>

<html lang="es">
<head>
<meta charset="UTF-8" />
<title>Herramienta de buscador</title>
<style type="text/css">
	body{
		font-family: Arail, sans-serif;
	}
	/* Formatting search box */
	.search-box{
		width: 300px;
		position: relative;
		display: inline-block;
		font-size: 14px;
	}

	.search-box input[type="text"]{
		height: 32px;
		padding: 5px 10px;
		border: 1px solid #CCCCCC;
		font-size: 14px;
	}

	.result{
		position: absolute;        
		z-index: 999;
		top: 100%;
		left: 0;
	}

	.search-box input[type="text"], .result{
		width: 100%;
		box-sizing: border-box;
	}
	/* Formatting result items */
	.result p{
		margin: 0;
		padding: 7px 10px;
		border: 1px solid #CCCCCC;
		border-top: none;
		cursor: pointer;
	}

	.result p:hover{
		background: #f2f2f2;
	}

</style>
<script src="../JS/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.search-box input[type="text"]').on("keyup input", function(){
		//Recoge la string introducida en el campo
		var term = $(this).val();
		var rs = $(this).siblings(".result");
		if(term.length){
			$.get("search-backend.php", {query: term}).done(function(data){
				//Coloca las búsquedas
				rs.html(data);
			});
		} else{
			rs.empty();
		}
	});

	//Coloca la opción seleccionada
	$(document).on("click", ".result p", function(){
		$(this).parents(".search-box").find('input[type="text"]').val($(this).text());
		$(this).parent(".result").empty();
	});

});

</script>

</head>

<body>
	<div class="search-box">
		<input type="text" autocomplete="off" placeholder="Buscar..." />
		<div class="result"></div>
	</div>

</body>

</html>

