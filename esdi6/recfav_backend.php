<?php
session_start();
include('utilidades/connection.php');
$id = intval($_GET['id']);
$usuarioid = intval($_SESSION['id']);
$consulta=sprintf("SELECT * FROM usuarios_has_recetas WHERE usuarios_id='%s' 
AND recetas_id ='%s'",
mysqli_real_escape_string($connexio, $_SESSION['id']),
mysqli_real_escape_string($connexio, $id));
$resultat=mysqli_query ($connexio, $consulta);
if (!$resultat){
	die ("ERROR: No se pudo ejecutar la consulta SQL");
}else {
	if(mysqli_num_rows($resultat) > 0){
		echo 'true';
		//Existe al darle, por lo que tenemos que borrar el registro
		$consultaDelete=sprintf("DELETE FROM usuarios_has_recetas WHERE usuarios_id='%s' 
		AND recetas_id ='%s'",
		mysqli_real_escape_string($connexio, $_SESSION['id']),
		mysqli_real_escape_string($connexio, $id));
		
		if (mysqli_query($connexio, $consultaDelete)) {
			//Todo correcto
		} else {
			echo "Hubo un error al actualizar tu favorito: " . mysqli_error($connexio);
		}
	}else{
		echo 'false';
		//No existe al darle, por lo que hay que crear el registro para tenerlo como favorito
		$consultaInsert=sprintf("INSERT INTO usuarios_has_recetas (usuarios_id, recetas_id) VALUES ('%d','%d')",
		mysqli_real_escape_string($connexio, $usuarioid),
		mysqli_real_escape_string($connexio, $id));
		if (mysqli_query($connexio, $consultaInsert)) {
			//Todo correcto
		} else {
			echo "Hubo un error al actualizar tu favorito: " . mysqli_error($connexio);
		}
	}
}
?>