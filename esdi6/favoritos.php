

		<?php include('header.php'); 
		 
		if(isset($_SESSION["user"])) {
			$conFavRec=sprintf("SELECT * FROM recetas JOIN usuarios_has_recetas ON (recetas.id = usuarios_has_recetas.recetas_id) WHERE usuarios_id='%s'",mysqli_real_escape_string($connexio, $_SESSION['id']));
			$conFavRes=sprintf("SELECT * FROM restaurantes JOIN usuarios_has_restaurantes ON (restaurantes.id = usuarios_has_restaurantes.restaurantes_id) WHERE usuarios_id='%s'",mysqli_real_escape_string($connexio, $_SESSION['id']));
			//Consultas viejas de pruebas -->
			$consultaRecetas = "SELECT * FROM recetas";
			$consultaRestaurantes = "SELECT * FROM restaurantes";
			//<--->
		}
		?>
		
		<div class="contentSection">
		<?php
		if(isset($_SESSION["user"])) {
		echo '<span class="search-titulo">Favoritos</span>
			<div class="contentFav">	
				<div class="fav-izq">
				<div class="fav-grupo">RECETAS</div>';
				
				
				$rsRec=mysqli_query($connexio,$conFavRec);
				if(!$rsRec){
					die ("No se pudo ejecutar la consulta SQL");
				}
				//Muestra recetas
				if(mysqli_num_rows($rsRec) > 0){
					while ($fila=mysqli_fetch_array_esdi($rsRec)){
						
						$oscurece = "";
						if (isset($_SESSION["id"])){
							$sqlAlergia = "SELECT recetas_id FROM recetas_has_ingredientes WHERE ingredientes_id = (SELECT ingredientes_id FROM alergias_has_ingredientes WHERE alergias_id = (SELECT alergias_id FROM usuarios_has_alergias WHERE usuarios_id = '".$_SESSION["id"]."'))";
							$rsAlergia=mysqli_query($connexio,$sqlAlergia);
							if(!$rsAlergia){
								die ("No se pudo ejecutar la consulta SQL de alergias.");
							}
							
							if(mysqli_num_rows($rsAlergia) > 0){
								$itera = 0;
								while ($fa=mysqli_fetch_array($rsAlergia)){
									$recetaAlergia[$itera] = $fa['recetas_id'];
									$itera++;
								}
								if(isset($recetaAlergia)){
									foreach ($recetaAlergia as $valor){
										if($valor == $fila['recetas.id']){
											$oscurece = "oscuro";
										}else $oscurece = "";
									}
									
								}
							}
						}
						
						$desc = substr($fila['recetas.descripcion'],0,130);
						echo "<div class='fav-item'><div class='search-item'>
						<div class='search-item-inside cl-effect-2'><a href='recetas.php?id=".$fila['recetas.id']."'><span data-hover='".$desc." [...]'><img class='search-item ".$oscurece."' alt='".$fila['recetas.nombre']."' src='".$fila['recetas.url_img']."'></span></a>
						<span class='search-item-name'>".$fila['recetas.nombre']."</span></div>
						</div></div>"; //<div class='cl-effect-2'>
					}
				}
		
			
				
				echo '</div>
				<div class="verticalLine">
				<div></div>
				</div>
				<div class="fav-izq">
				<div class="fav-grupo">RESTAURANTES</div>';
				
				$rsRes=mysqli_query($connexio,$conFavRes);
				if(!$rsRes){
					die ("No se pudo ejecutar la consulta SQL");
				}
				//Muestra recetas
				if(mysqli_num_rows($rsRes) > 0){
					while ($fila=mysqli_fetch_array_esdi($rsRes)){
						$desc = substr($fila['restaurantes.descripcion'],0,130);
						echo "<div class='fav-item'><div class='search-item'>
						<div class='search-item-inside cl-effect-2'><a href='restaurantes.php?id=".$fila['restaurantes.id']."'><span data-hover='".$desc." [...]'><img class='search-item' src='".$fila['restaurantes.url_img']."'></span></a>
						<span class='search-item-name'>".$fila['restaurantes.nombre']."</span></div>
						</div></div>";
					}
				}
			
				
				
			echo '	</div>
			</div>
				
			</div>';
		}
		?>
		<?php include('modal.php'); ?>
		<?php include('footer.php'); ?>

	
