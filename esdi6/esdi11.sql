-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-09-2017 a las 16:58:54
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `esdi`
--
DROP DATABASE `esdi`;
CREATE DATABASE `esdi` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `esdi`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alergias`
--

DROP TABLE IF EXISTS `alergias`;
CREATE TABLE IF NOT EXISTS `alergias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `alergias`
--

INSERT INTO `alergias` (`id`, `nombre`) VALUES
(1, 'Arroz'),
(2, 'Frutas y verduras'),
(3, 'Marisco'),
(4, 'Cereales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alergias_has_ingredientes`
--

DROP TABLE IF EXISTS `alergias_has_ingredientes`;
CREATE TABLE IF NOT EXISTS `alergias_has_ingredientes` (
  `alergias_id` int(11) NOT NULL,
  `ingredientes_id` int(11) NOT NULL,
  PRIMARY KEY (`alergias_id`,`ingredientes_id`),
  KEY `fk_alergias_has_ingredientes_ingredientes1_idx` (`ingredientes_id`),
  KEY `fk_alergias_has_ingredientes_alergias1_idx` (`alergias_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcar la base de datos para la tabla `alergias_has_ingredientes`
--

INSERT INTO `alergias_has_ingredientes` (`alergias_id`, `ingredientes_id`) VALUES
(3, 315);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_ingredientes`
--

DROP TABLE IF EXISTS `categoria_ingredientes`;
CREATE TABLE IF NOT EXISTS `categoria_ingredientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `url_img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `categoria_ingredientes`
--

INSERT INTO `categoria_ingredientes` (`id`, `nombre`, `url_img`) VALUES
(1, 'Cereal', 'images/cereal.png'),
(2, 'Neutro', 'images/neutro.png'),
(3, 'China / India', 'images/tag.png'),
(4, 'Glucosa y fructosa', 'images/cereal.png'),
(5, 'Dulce', 'images/azucar.png'),
(6, 'América y otros', 'images/tag.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_recetas`
--

DROP TABLE IF EXISTS `categoria_recetas`;
CREATE TABLE IF NOT EXISTS `categoria_recetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `url_img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `categoria_recetas`
--

INSERT INTO `categoria_recetas` (`id`, `nombre`, `url_img`) VALUES
(1, 'Sushi', 'images/prueba.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_restaurantes`
--

DROP TABLE IF EXISTS `categoria_restaurantes`;
CREATE TABLE IF NOT EXISTS `categoria_restaurantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `categoria_restaurantes`
--

INSERT INTO `categoria_restaurantes` (`id`, `nombre`) VALUES
(1, 'Japoneses');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE IF NOT EXISTS `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `regiones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ciudad_regiones1_idx` (`regiones_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcar la base de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id`, `nombre`, `regiones_id`) VALUES
(4, 'Sabadell', 1),
(5, 'Barcelona', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comentario` varchar(1000) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `recetas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comentarios_recetas1_idx` (`recetas_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=118 ;

--
-- Volcar la base de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `comentario`, `nombre`, `fecha`, `recetas_id`) VALUES
(1, 'blabla', 'nick', '2017-02-12', 1),
(111, 'Que rico! Lo intentare mañana :)', 'Maria F.', '2016-12-20', 3),
(112, 'Hola, yo lo compro en lomos sin problemas. En Hipercor, para mí, calidad excelente y te lo limpian de la piel perfectamente', 'Carlos S.', '2016-12-16', 3),
(113, 'No me parece difícil, sí laborioso... Me animaré a hacerlo, claro que sí, cuando vengan mis hijas, a las que les encanta el sushi :-) Gracias por la explicación.', 'Marta M.', '2016-12-20', 2),
(114, 'Qué buenos! Mis preferidos son los "Toro" (ventresca de atún) y los "Unagui" (anguila), lo malo es que ésta última no hay manera de conseguirla aquí en Málaga, así que cuando voy a Madrid me traigo un buen cargamento del Tokio Ya... Por cierto, que ya me va tocando ir!<br>Saludos', 'Carlos R.', '2016-12-16', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios_restaurantes`
--

DROP TABLE IF EXISTS `comentarios_restaurantes`;
CREATE TABLE IF NOT EXISTS `comentarios_restaurantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comentario` varchar(1000) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `restaurantes_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comentarios_restaurantes_restaurantes1_idx` (`restaurantes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcar la base de datos para la tabla `comentarios_restaurantes`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `continentes`
--

DROP TABLE IF EXISTS `continentes`;
CREATE TABLE IF NOT EXISTS `continentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `continentes`
--

INSERT INTO `continentes` (`id`, `nombre`) VALUES
(1, 'Europa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingredientes`
--

DROP TABLE IF EXISTS `ingredientes`;
CREATE TABLE IF NOT EXISTS `ingredientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `url_img` varchar(200) DEFAULT NULL,
  `cantidad` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=317 ;

--
-- Volcar la base de datos para la tabla `ingredientes`
--

INSERT INTO `ingredientes` (`id`, `nombre`, `url_img`, `cantidad`) VALUES
(31, 'Arroz', 'images/arroz.png', '500 gr'),
(32, 'Alga', '', '50 gr'),
(33, 'Salmón', '', '100 gr'),
(34, 'Aguacate', '', '1/2 pieza'),
(311, 'Arroz', 'images/dados.png', '300 gr'),
(312, 'Agua', '', '350 ml'),
(313, 'Vinagre de arroz', '', '2 cucharadas'),
(314, 'Sal', '', '1/4 cucharadita'),
(315, 'Salmón, pez mantequilla, atún, gambas,etc. (Depende de ti)', '', '100 gr'),
(316, 'Azúcar', '', '1 cucharadita');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingredientes_has_categoria_ingredientes`
--

DROP TABLE IF EXISTS `ingredientes_has_categoria_ingredientes`;
CREATE TABLE IF NOT EXISTS `ingredientes_has_categoria_ingredientes` (
  `ingredientes_id` int(11) NOT NULL,
  `categoria_ingredientes_id` int(11) NOT NULL,
  PRIMARY KEY (`ingredientes_id`,`categoria_ingredientes_id`),
  KEY `fk_ingredientes_has_categoria_ingredientes_categoria_ingred_idx` (`categoria_ingredientes_id`),
  KEY `fk_ingredientes_has_categoria_ingredientes_ingredientes1_idx` (`ingredientes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `ingredientes_has_categoria_ingredientes`
--

INSERT INTO `ingredientes_has_categoria_ingredientes` (`ingredientes_id`, `categoria_ingredientes_id`) VALUES
(31, 1),
(31, 2),
(31, 3),
(316, 4),
(316, 5),
(316, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingredientes_has_paises`
--

DROP TABLE IF EXISTS `ingredientes_has_paises`;
CREATE TABLE IF NOT EXISTS `ingredientes_has_paises` (
  `ingredientes_id` int(11) NOT NULL,
  `paises_id` int(11) NOT NULL,
  PRIMARY KEY (`ingredientes_id`,`paises_id`),
  KEY `fk_ingredientes_has_paises_paises1_idx` (`paises_id`),
  KEY `fk_ingredientes_has_paises_ingredientes1_idx` (`ingredientes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `ingredientes_has_paises`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intolerancias`
--

DROP TABLE IF EXISTS `intolerancias`;
CREATE TABLE IF NOT EXISTS `intolerancias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `intolerancias`
--

INSERT INTO `intolerancias` (`id`, `nombre`) VALUES
(1, 'Gluten');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `continentes_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_paises_continentes1_idx` (`continentes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `nombre`, `continentes_id`) VALUES
(1, 'España', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises_has_recetas`
--

DROP TABLE IF EXISTS `paises_has_recetas`;
CREATE TABLE IF NOT EXISTS `paises_has_recetas` (
  `paises_id` int(11) NOT NULL,
  `recetas_id` int(11) NOT NULL,
  PRIMARY KEY (`paises_id`,`recetas_id`),
  KEY `fk_paises_has_recetas_recetas1_idx` (`recetas_id`),
  KEY `fk_paises_has_recetas_paises1_idx` (`paises_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `paises_has_recetas`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

DROP TABLE IF EXISTS `recetas`;
CREATE TABLE IF NOT EXISTS `recetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `url_img` varchar(200) NOT NULL,
  `url_video` varchar(200) NOT NULL,
  `descripcion` varchar(10000) DEFAULT NULL,
  `categoria_recetas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_recetas_categoria_recetas1_idx` (`categoria_recetas_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id`, `nombre`, `url_img`, `url_video`, `descripcion`, `categoria_recetas_id`) VALUES
(1, 'Sushi', 'images/prueba.jpg', 'https://www.youtube.com/embed/AHLt7g8j_Ks', 'Descripción del proceso de un sushi', 1),
(2, 'Nigiri', 'images/nigiri.jpg', 'https://www.youtube.com/embed/4KktvArKgCI', '1. La cantidad de arroz indicada es la que normalmente hacemos en casa para preparar tres maki-sushi (18 piezas) y unos 10 nigiri-sushi. En primer lugar preparamos el arroz con suficiente antelación para que esté a temperatura ambiente. En un colador ponemos el arroz glutinoso y lo escurrimos con agua hasta que esta salga clara. En una cazuela colocamos el arroz con el agua y dejamos que se hidrate durante diez minutos. Pasado este tiempo, ponemos a fuego fuerte y cuando entre en ebullición, bajamos el fuego al mínimo, tapamos la cazuela y dejamos cocer diez minutos. Retiramos la cazuela del fuego y dejamos reposar otros quince minutos a cazuela tapada.<br>\r\n\r\n2. Mientras tanto ponemos el vinagre de arroz, el azúcar y la sal a calentar en un cacito hasta que se disuelva, y reservamos. Colocamos el arroz en un recipiente amplio y agregamos el condimento de vinagre. Enfriamos el arroz con un abanico (o con un secador en posición frío y baja potencia) y lo vamos moviendo suavemente con una espátula de madera para repartir el aliño de manera homogénea.<br>\r\n\r\n3. Cuando el arroz esté a temperatura ambiente, procedemos a formar los nigiri-sushi. Disponemos un bol con agua y una cucharadita de vinagre de arroz, para ir mojando las manos según trabajamos las piezas. Con las manos humedecidas, tomamos una porción de arroz y la compactamos apretando los dedos sobre la palma de la mano. Obtendremos una pequeña masa a la cual daremos forma utilizando el dedo índice de la mano contraria hasta que tenga un aspecto uniforme y el arroz no se despegue, según se observa en la imagen.<br>\r\n\r\n4. Cuando tengamos los nigiris formados, añadimos una lámina de pescado previamente untada con un poco de wasabi (con moderación, pues es muy picante) y la colocamos en la parte superior. El pescado debe estar previamente congelado y cortado en pequeñas láminas finas, por lo que es preferible comprarlo ya preparado en una tienda de productos japoneses. Haremos una tortilla japonesa según nuestra receta, y la cortaremos a lo ancho en láminas muy finas, que colocaremos encima de los nigiris. Cortamos un tira de alga nori de medio centímetro, y rodeamos el nigiri de tortilla con ella, dejando que los extremos queden en la parte inferior de este, humedeciéndolos ligeramente para que queden bien fijados.\r\n', 1),
(3, 'Sashimi', 'images/sashimi.jpg', 'https://www.youtube.com/embed/A40Wxs6ex7Y&t=4s', '1. Con una servilleta húmeda limpiamos el salmón de escamas sueltas que pueda tener. Con las yemas de los dedos buscamos posibles espinas que pueda tener (notaremos algo duro entre la carne suave) y pellizcándolas tiramos de ellas para extraerlas.<br>\r\n\r\n2. Con el cuchillo separamos la rodaja en dos lomos eliminando la espina dorsal. Tambien nos deshacemos de la fila de espinas que tiene en su cara interna.<br>\r\n\r\n3. Colocamos cada lomo con la piel hacia arriba. Con ayuda del cuchillo y vamos separando la carne de la piel mientras tiramos de ella con la otra mano. Una vez conseguido ya tenemos nuestros lomos limpios! Esta pieza se podría usar también para realizar nuestros maki-rolls preferidos.<br>\r\n\r\n4. Por último colocamos el cuchillo a 45 grados y comenzamos a filetear cada lomo. El movimiento debe ser continuo, por lo que empezamos con la base de la hoja y terminamos en la punta. PROHIBIDO hacer el mismo movimiento que al cortar pan!\r\nDisponemos los filetes del modo que más nos guste y los acompañamos con un poco de wasabi.\r\n', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas_has_ingredientes`
--

DROP TABLE IF EXISTS `recetas_has_ingredientes`;
CREATE TABLE IF NOT EXISTS `recetas_has_ingredientes` (
  `recetas_id` int(11) NOT NULL,
  `ingredientes_id` int(11) NOT NULL,
  PRIMARY KEY (`recetas_id`,`ingredientes_id`),
  KEY `fk_recetas_has_ingredientes_ingredientes1_idx` (`ingredientes_id`),
  KEY `fk_recetas_has_ingredientes_recetas1_idx` (`recetas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `recetas_has_ingredientes`
--

INSERT INTO `recetas_has_ingredientes` (`recetas_id`, `ingredientes_id`) VALUES
(1, 31),
(3, 31),
(3, 32),
(3, 33),
(3, 34),
(2, 311),
(2, 312),
(2, 313),
(2, 314),
(2, 315),
(2, 316);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regiones`
--

DROP TABLE IF EXISTS `regiones`;
CREATE TABLE IF NOT EXISTS `regiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `paises_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_region_paises1_idx` (`paises_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `regiones`
--

INSERT INTO `regiones` (`id`, `nombre`, `paises_id`) VALUES
(1, 'Cataluña', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurantes`
--

DROP TABLE IF EXISTS `restaurantes`;
CREATE TABLE IF NOT EXISTS `restaurantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `paises_id` int(11) NOT NULL,
  `categoria_restaurantes_id` int(11) NOT NULL,
  `url_img` varchar(200) DEFAULT NULL,
  `descripcion` varchar(1000) DEFAULT NULL,
  `url_map` varchar(1000) DEFAULT NULL,
  `regiones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_restaurantes_paises1_idx` (`paises_id`),
  KEY `fk_restaurantes_categoria_restaurantes1_idx` (`categoria_restaurantes_id`),
  KEY `fk_restaurantes_region1_idx` (`regiones_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `restaurantes`
--

INSERT INTO `restaurantes` (`id`, `nombre`, `paises_id`, `categoria_restaurantes_id`, `url_img`, `descripcion`, `url_map`, `regiones_id`) VALUES
(1, 'Kaitensushi', 1, 1, 'images/Kaitensushi.png', 'El kaitensushi es un restaurante de sushi donde los platos se colocan sobre un transportador de banda que atraviesa por el restaurante y pasa por cada mesa y sillas de los comensales. Los comensales pueden pedir alguna orden especial, pero pueden simplemente tomar el platillo de su elección de un flujo constante de sushi fresco que se mueve a través de un transportador de banda. La cuenta final se calcula en base en el número y tipo de platillos que se hayan consumido. Además de la banda transportadora, algunos restaurantes usan una presentación más elegante que es la de utilizar pequeños botes de madera viajando a través de pequeños canales o trenes en miniatura.', NULL, 1),
(2, 'Udon', 1, 1, 'images/foto2.jpg', 'UDON es una cadena de restaurantes Noodle Bars especializada en la elaboración de fideos orientales. Nuestra inspiración es el concepto casual dining, que combina la filosofía de la comida rápida (velocidad y precio competitivo) con la calidad de la materia prima y el servicio de la cocina tradicional. ', 'www.google.es/maps/search/udon+/@41.5465752,2.0723273,13z/data=!3m1!4b1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) CHARACTER SET utf8 NOT NULL,
  `apellidos` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(45) CHARACTER SET utf8 NOT NULL,
  `password` char(128) CHARACTER SET utf8 NOT NULL COMMENT 'Password en SHA512',
  `edad` int(11) DEFAULT NULL,
  `paises_id` int(11) NOT NULL,
  `ciudades_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_paises1_idx` (`paises_id`),
  KEY `fk_usuarios_ciudad1_idx` (`ciudades_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Usuarios suscriptores' AUTO_INCREMENT=6 ;

--
-- Volcar la base de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `alias`, `password`, `edad`, `paises_id`, `ciudades_id`) VALUES
(1, 'José', 'Botella', 'Pepe', 'e83e8535d6f689493e5819bd60aa3e5fdcba940e6d111ab6fb5c34f24f86496bf3726e2bf4ec59d6d2f5a2aeb1e4f103283e7d64e4f49c03b4c4725cb361e773', 20, 1, 4),
(2, 'Francisco', 'Ríos', 'fran', 'e83e8535d6f689493e5819bd60aa3e5fdcba940e6d111ab6fb5c34f24f86496bf3726e2bf4ec59d6d2f5a2aeb1e4f103283e7d64e4f49c03b4c4725cb361e773', 25, 1, 5),
(4, 'prueba', 'prueba2', 'lanif', 'e83e8535d6f689493e5819bd60aa3e5fdcba940e6d111ab6fb5c34f24f86496bf3726e2bf4ec59d6d2f5a2aeb1e4f103283e7d64e4f49c03b4c4725cb361e773', 222, 1, 4),
(5, 'pepe2', 'palomar', 'pepe2', 'e83e8535d6f689493e5819bd60aa3e5fdcba940e6d111ab6fb5c34f24f86496bf3726e2bf4ec59d6d2f5a2aeb1e4f103283e7d64e4f49c03b4c4725cb361e773', 12, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_has_alergias`
--

DROP TABLE IF EXISTS `usuarios_has_alergias`;
CREATE TABLE IF NOT EXISTS `usuarios_has_alergias` (
  `usuarios_id` int(11) NOT NULL,
  `alergias_id` int(11) NOT NULL,
  PRIMARY KEY (`usuarios_id`,`alergias_id`),
  KEY `fk_usuarios_has_alergias_alergias1_idx` (`alergias_id`),
  KEY `fk_usuarios_has_alergias_usuarios1_idx` (`usuarios_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcar la base de datos para la tabla `usuarios_has_alergias`
--

INSERT INTO `usuarios_has_alergias` (`usuarios_id`, `alergias_id`) VALUES
(5, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_has_intolerancias`
--

DROP TABLE IF EXISTS `usuarios_has_intolerancias`;
CREATE TABLE IF NOT EXISTS `usuarios_has_intolerancias` (
  `usuarios_id` int(11) NOT NULL,
  `intolerancias_id` int(11) NOT NULL,
  PRIMARY KEY (`usuarios_id`,`intolerancias_id`),
  KEY `fk_usuarios_has_intolerancias_intolerancias1_idx` (`intolerancias_id`),
  KEY `fk_usuarios_has_intolerancias_usuarios1_idx` (`usuarios_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcar la base de datos para la tabla `usuarios_has_intolerancias`
--

INSERT INTO `usuarios_has_intolerancias` (`usuarios_id`, `intolerancias_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_has_recetas`
--

DROP TABLE IF EXISTS `usuarios_has_recetas`;
CREATE TABLE IF NOT EXISTS `usuarios_has_recetas` (
  `usuarios_id` int(11) NOT NULL,
  `recetas_id` int(11) NOT NULL,
  PRIMARY KEY (`usuarios_id`,`recetas_id`),
  KEY `fk_usuarios_has_recetas_recetas1_idx` (`recetas_id`),
  KEY `fk_usuarios_has_recetas_usuarios1_idx` (`usuarios_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcar la base de datos para la tabla `usuarios_has_recetas`
--

INSERT INTO `usuarios_has_recetas` (`usuarios_id`, `recetas_id`) VALUES
(1, 1),
(2, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_has_restaurantes`
--

DROP TABLE IF EXISTS `usuarios_has_restaurantes`;
CREATE TABLE IF NOT EXISTS `usuarios_has_restaurantes` (
  `usuarios_id` int(11) NOT NULL,
  `restaurantes_id` int(11) NOT NULL,
  PRIMARY KEY (`usuarios_id`,`restaurantes_id`),
  KEY `fk_usuarios_has_restaurantes_restaurantes1_idx` (`restaurantes_id`),
  KEY `fk_usuarios_has_restaurantes_usuarios1_idx` (`usuarios_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcar la base de datos para la tabla `usuarios_has_restaurantes`
--

INSERT INTO `usuarios_has_restaurantes` (`usuarios_id`, `restaurantes_id`) VALUES
(1, 1),
(2, 1),
(5, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votos_recetas`
--

DROP TABLE IF EXISTS `votos_recetas`;
CREATE TABLE IF NOT EXISTS `votos_recetas` (
  `recetas_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `voto` int(11) NOT NULL,
  PRIMARY KEY (`recetas_id`,`usuarios_id`),
  KEY `fk_recetas_has_usuarios_usuarios1_idx` (`usuarios_id`),
  KEY `fk_recetas_has_usuarios_recetas1_idx` (`recetas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `votos_recetas`
--

INSERT INTO `votos_recetas` (`recetas_id`, `usuarios_id`, `voto`) VALUES
(1, 1, 5),
(3, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votos_restaurantes`
--

DROP TABLE IF EXISTS `votos_restaurantes`;
CREATE TABLE IF NOT EXISTS `votos_restaurantes` (
  `restaurantes_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `voto` int(11) NOT NULL,
  PRIMARY KEY (`restaurantes_id`,`usuarios_id`),
  KEY `fk_restaurantes_has_usuarios_usuarios1_idx` (`usuarios_id`),
  KEY `fk_restaurantes_has_usuarios_restaurantes1_idx` (`restaurantes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `votos_restaurantes`
--

INSERT INTO `votos_restaurantes` (`restaurantes_id`, `usuarios_id`, `voto`) VALUES
(1, 5, 3),
(2, 1, 2);

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `alergias_has_ingredientes`
--
ALTER TABLE `alergias_has_ingredientes`
  ADD CONSTRAINT `fk_alergias_has_ingredientes_alergias1` FOREIGN KEY (`alergias_id`) REFERENCES `alergias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_alergias_has_ingredientes_ingredientes1` FOREIGN KEY (`ingredientes_id`) REFERENCES `ingredientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD CONSTRAINT `fk_ciudad_regiones1` FOREIGN KEY (`regiones_id`) REFERENCES `regiones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `fk_comentarios_recetas1` FOREIGN KEY (`recetas_id`) REFERENCES `recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentarios_restaurantes`
--
ALTER TABLE `comentarios_restaurantes`
  ADD CONSTRAINT `fk_comentarios_restaurantes_restaurantes1` FOREIGN KEY (`restaurantes_id`) REFERENCES `restaurantes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ingredientes_has_categoria_ingredientes`
--
ALTER TABLE `ingredientes_has_categoria_ingredientes`
  ADD CONSTRAINT `fk_ingredientes_has_categoria_ingredientes_categoria_ingredie1` FOREIGN KEY (`categoria_ingredientes_id`) REFERENCES `categoria_ingredientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ingredientes_has_categoria_ingredientes_ingredientes1` FOREIGN KEY (`ingredientes_id`) REFERENCES `ingredientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ingredientes_has_paises`
--
ALTER TABLE `ingredientes_has_paises`
  ADD CONSTRAINT `fk_ingredientes_has_paises_ingredientes1` FOREIGN KEY (`ingredientes_id`) REFERENCES `ingredientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ingredientes_has_paises_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `paises`
--
ALTER TABLE `paises`
  ADD CONSTRAINT `fk_paises_continentes1` FOREIGN KEY (`continentes_id`) REFERENCES `continentes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `paises_has_recetas`
--
ALTER TABLE `paises_has_recetas`
  ADD CONSTRAINT `fk_paises_has_recetas_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_paises_has_recetas_recetas1` FOREIGN KEY (`recetas_id`) REFERENCES `recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD CONSTRAINT `fk_recetas_categoria_recetas1` FOREIGN KEY (`categoria_recetas_id`) REFERENCES `categoria_recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recetas_has_ingredientes`
--
ALTER TABLE `recetas_has_ingredientes`
  ADD CONSTRAINT `fk_recetas_has_ingredientes_ingredientes1` FOREIGN KEY (`ingredientes_id`) REFERENCES `ingredientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recetas_has_ingredientes_recetas1` FOREIGN KEY (`recetas_id`) REFERENCES `recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `regiones`
--
ALTER TABLE `regiones`
  ADD CONSTRAINT `fk_region_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  ADD CONSTRAINT `fk_restaurantes_categoria_restaurantes1` FOREIGN KEY (`categoria_restaurantes_id`) REFERENCES `categoria_restaurantes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restaurantes_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restaurantes_region1` FOREIGN KEY (`regiones_id`) REFERENCES `regiones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_ciudad1` FOREIGN KEY (`ciudades_id`) REFERENCES `ciudades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_paises1` FOREIGN KEY (`paises_id`) REFERENCES `paises` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios_has_alergias`
--
ALTER TABLE `usuarios_has_alergias`
  ADD CONSTRAINT `fk_usuarios_has_alergias_alergias1` FOREIGN KEY (`alergias_id`) REFERENCES `alergias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_alergias_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios_has_intolerancias`
--
ALTER TABLE `usuarios_has_intolerancias`
  ADD CONSTRAINT `fk_usuarios_has_intolerancias_intolerancias1` FOREIGN KEY (`intolerancias_id`) REFERENCES `intolerancias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_intolerancias_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios_has_recetas`
--
ALTER TABLE `usuarios_has_recetas`
  ADD CONSTRAINT `fk_usuarios_has_recetas_recetas1` FOREIGN KEY (`recetas_id`) REFERENCES `recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_recetas_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios_has_restaurantes`
--
ALTER TABLE `usuarios_has_restaurantes`
  ADD CONSTRAINT `fk_usuarios_has_restaurantes_restaurantes1` FOREIGN KEY (`restaurantes_id`) REFERENCES `restaurantes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_restaurantes_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `votos_recetas`
--
ALTER TABLE `votos_recetas`
  ADD CONSTRAINT `fk_recetas_has_usuarios_recetas1` FOREIGN KEY (`recetas_id`) REFERENCES `recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recetas_has_usuarios_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `votos_restaurantes`
--
ALTER TABLE `votos_restaurantes`
  ADD CONSTRAINT `fk_restaurantes_has_usuarios_restaurantes1` FOREIGN KEY (`restaurantes_id`) REFERENCES `restaurantes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restaurantes_has_usuarios_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
