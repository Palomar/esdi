<?php
$lblform = array( array("Nombre","Apellidos","Nombre de usuario","Contraseña","Edad","País","Alergias"),
                 array("Nom","Cognoms","Nom d'usuari","Contrasenya","Edat","Pais","Alergies"));
$lblbienvenida = array( array("ENHORABUENA","ya formas parte de nuestra familia"),
                array("ENHORABONA","ja formas part de la nostra familia"));
$login = array(array("Usuario","Contraseña"),array("Usuari","Contrasenya"));
$idioma = 0


                ?>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
        <div id ="m2" style="display:none;">
          <form action="index.php" method="post">

          <h2>Registro</h2>
          <div class="labelsModal">
              <span class="spanmodal"><?php echo $lblform[$idioma][0]; ?></span>            <!-- Nomnre -->
              <input id="inpt_nombre" type="text" name="nombre" required="true">
              <span class="spanmodal"><?php echo $lblform[$idioma][1]; ?></span>            <!-- Apellidos -->
              <input id="inpt_apellidos" type="text" name="apellido"  required="true">

              <span id="block" class="spanmodal"><?php echo $lblform[$idioma][2]; ?></span>            <!-- N Usuario -->
              <input id="inpt_nick" type="text" name="nick" required="true">

              <span id="span_separacion" class="spanmodal"><?php echo $lblform[$idioma][3]; ?></span>  <!-- Contraseña -->
              <input id="inpt_pass" type="password" name="pass" required="true">

              <span class="spanmodal"><?php echo $lblform[$idioma][4]; ?></span>            <!-- Edad -->
              <input id="inpt_edad" type="text" name="edad" required="true">

              <span class="spanmodal"><?php echo $lblform[$idioma][5]; ?></span>            <!-- Pais -->
              <input id="inpt_pais" type="text" name="pais" required="true">

              <span class="spanmodal"><?php echo $lblform[$idioma][6]; ?></span>             <!-- Ciudad -->

              <select  name="alergias" id="alergias" required="true">
                <option value="" default>Ninguna</option>
                <?php

              $consulta='SELECT * FROM alergias';
              $resultat=mysqli_query ($connexio, $consulta);
              if (!$resultat) die ("ERROR: No se pudo ejecutar la consulta SQL para rellenar el campo con las alergias");
                while($fila=mysqli_fetch_array ($resultat))
                {
                    $id = $fila["id"];
                    $titulo = $fila["nombre"];

                echo'  <option value='.$id.' default>'.$titulo.'</option>';
                }
              ?>
              </select>

              <div style ="width:70%;display:flex;justify-content:end;">
              <button type ="submit" class="btnModal" >

              </div>
            </div>

          </form>
        </div>
        <div id="m3" style="display:none;">
          <div class="txt_bienvenida">
              <p><?php echo $lblbienvenida[$idioma][0]; ?></p>

            </div>
            <div class="txt_bienvenida">
              <p><?php echo $lblbienvenida[$idioma][1]; ?></p>
            </div>
         </div>
         <div id="m1" style="display:none;">
          <form action="index.php" method="post">
           <div style="display:flex;justify-content:center;flex-wrap:wrap;">
              <h2>Login</h2>
               <div class="input_spanLogin">
                   <span class="spanmodal"><?php echo $login[$idioma][0]; ?></span>            <!-- user -->
                   <input id="txt_user" type="text" name="userLogin" required="true">
                   <span class="spanmodal"><?php echo $login[$idioma][1]; ?></span>            <!-- pass -->
                   <input id="txt_pass" type="password" name="passLogin"  required="true" style="margin-bottom:20px;">
                   <div style ="width:100%;display:flex;justify-content:end;">
                     <button type ="submit" class="btnModal" ></span>
                   </div>
              </div>
            </div>
          </form>
         </div>
  </div>

</div>
