<?php include('header.php'); ?>
		<div class="contentSection">
		<span class="search-titulo">Resultado de búsqueda</span>
<div class="search-show">
<script>
//Para recolocar la lista de platos independientemente del tamaño de pantalla ocupando todo el espacio
$(document).ready(function(){
	$(".main").css("max-width", "98%"); 
});
</script>
<?php
//SELECT recetas_id FROM recetas_has_ingredientes WHERE ingredientes_id = (SELECT ingredientes_id FROM alergias_has_ingredientes WHERE alergias_id = (SELECT alergias_id FROM usuarios_has_alergias WHERE usuarios_id = 5))
if(isset($_GET['search'])){
	$search = $_GET['search'];

	$consultaRecetas = "SELECT * FROM recetas WHERE nombre LIKE '" . $search . "%'";
	$consultaRestaurantes = "SELECT * FROM restaurantes WHERE nombre LIKE '" . $search . "%'";
	$vacio = true; //Para poner un mensaje sin resultados
	$rsRec=mysqli_query($connexio,$consultaRecetas);
	if(!$rsRec){
		die ("No se pudo ejecutar la consulta SQL");
	}
	//Muestra recetas
	if(mysqli_num_rows($rsRec) > 0){
		$vacio = false;
		$iterarec = 0;
		while ($fila=mysqli_fetch_array_esdi($rsRec)){
			$oscurece = "";
			if (isset($_SESSION["id"])){
				$sqlAlergia = "SELECT recetas_id FROM recetas_has_ingredientes WHERE ingredientes_id = (SELECT ingredientes_id FROM alergias_has_ingredientes WHERE alergias_id = (SELECT alergias_id FROM usuarios_has_alergias WHERE usuarios_id = '".$_SESSION["id"]."'))";
				$rsAlergia=mysqli_query($connexio,$sqlAlergia);
				if(!$rsAlergia){
					die ("No se pudo ejecutar la consulta SQL de alergias.");
				}
				
				if(mysqli_num_rows($rsAlergia) > 0){
					$itera = 0;
					while ($fa=mysqli_fetch_array($rsAlergia)){
						$recetaAlergia[$itera] = $fa['recetas_id'];
						$itera++;
					}
					if(isset($recetaAlergia)){
						foreach ($recetaAlergia as $valor){
							if($valor == $fila['recetas.id']){
								$oscurece = "oscuro";
							}else $oscurece = "";
						}
						
					}
				}
			}
			$desc = substr($fila['recetas.descripcion'],0,130);
			echo "<div class='search-item'>
			<div class='search-item-inside cl-effect-2'><a href='recetas.php?id=".$fila['recetas.id']."'><span data-hover='".$desc." [...]'>";
			
			echo "<img class='search-item ".$oscurece."' alt='".$fila['recetas.nombre']."' src='".$fila['recetas.url_img']."'></span></a>
			<span class='search-item-name'>".$fila['recetas.nombre']."</span></div>
			</div>";
			/* echo $fila['recetas.id'];
			echo " ".$fila['recetas.nombre'];
			echo " ".$fila['recetas.url_img'];
			echo " ".$fila['recetas.url_video'];
			echo " ".$fila['recetas.descripcion'];
			echo " ".$fila['recetas.categoria_recetas_id']; */


		}
	}

	$rsRes=mysqli_query($connexio,$consultaRestaurantes);
	if(!$rsRes){
		die ("No se pudo ejecutar la consulta SQL");
	}
	//Muestra restaurantes
	if(mysqli_num_rows($rsRes) > 0){
		$vacio = false;
		while ($fila=mysqli_fetch_array_esdi($rsRes)){
			$desc = substr($fila['restaurantes.descripcion'],0,150);
			/* echo "<br>"; */
			echo "<div class='search-item'>
			<div class='search-item-inside cl-effect-2'><a href='restaurantes.php?id=".$fila['restaurantes.id']."'><span data-hover='".$desc." [...]'><img class='search-item' src='".$fila['restaurantes.url_img']."'></span></a>
			<span class='search-item-name'>".$fila['restaurantes.nombre']."</span></div>
			</div>";
			/*echo "<br>";
			echo $fila['restaurantes.id'];
			echo " ".$fila['restaurantes.nombre'];
			echo " ".$fila['restaurantes.url_img'];
			echo " ".$fila['restaurantes.url_map'];
			echo " ".$fila['restaurantes.descripcion'];
			echo " ".$fila['restaurantes.categoria_restaurantes_id'];
			echo " ".$fila['restaurantes.paises_id'];
			echo " ".$fila['restaurantes.regiones_id'];*/

		}


	}

	if ($vacio){
		echo "<div class='search-show'>No hay resultados para dicha búsqueda.</div>";
	}
}

?>
</div></div>


<?php include('modal.php'); ?>
<?php include('footer.php'); ?>
