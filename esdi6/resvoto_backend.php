<?php
session_start();
include('utilidades/connection.php');
$id = intval($_GET['id']);
$voto = intval($_GET['voto']);
$usuarioid = intval($_SESSION['id']);
$consulta=sprintf("SELECT * FROM votos_restaurantes WHERE usuarios_id='%s' 
AND restaurantes_id ='%s'",
mysqli_real_escape_string($connexio, $_SESSION['id']),
mysqli_real_escape_string($connexio, $id));
$resultat=mysqli_query ($connexio, $consulta);
if (!$resultat){
	die ("ERROR: No se pudo ejecutar la consulta SQL");
}else {
	if(mysqli_num_rows($resultat) > 0){
		echo 'true';
		//Existe al darle, por lo que tenemos que borrar el registro
		$consultaDelete=sprintf("UPDATE votos_restaurantes SET voto='%s' WHERE usuarios_id='%s' 
		AND restaurantes_id ='%s'",
		mysqli_real_escape_string($connexio, $voto),
		mysqli_real_escape_string($connexio, $_SESSION['id']),
		mysqli_real_escape_string($connexio, $id));
		
		if (mysqli_query($connexio, $consultaDelete)) {
			//Todo correcto
			echo ',';
			$consultaPuntos = "SELECT round((SELECT sum(voto) FROM votos_restaurantes WHERE restaurantes_id = '".$id."') / (SELECT count(*) FROM votos_restaurantes WHERE restaurantes_id = '".$id."'),2)";
			$res=mysqli_query($connexio,$consultaPuntos);
			if(!$res){
				die ("No se pudo ejecutar la consulta SQL de puntos de valoración.");
			}
			while($f=mysqli_fetch_row($res)){
				echo $f[0];
				echo '★';
			}
		} else {
			echo "Hubo un error al actualizar tu favorito: " . mysqli_error($connexio);
		}
	}else{
		echo 'false';
		//No existe al darle, por lo que hay que crear el registro para tenerlo como favorito
		$consultaInsert=sprintf("INSERT INTO votos_restaurantes (usuarios_id, restaurantes_id,voto) VALUES ('%d','%d','%d')",
		mysqli_real_escape_string($connexio, $usuarioid),
		mysqli_real_escape_string($connexio, $id),
		mysqli_real_escape_string($connexio, $voto));
		if (mysqli_query($connexio, $consultaInsert)) {
			//Todo correcto
			echo ',';
			$consultaPuntos = "SELECT round((SELECT sum(voto) FROM votos_restaurantes WHERE restaurantes_id = '".$id."') / (SELECT count(*) FROM votos_restaurantes WHERE restaurantes_id = '".$id."'),2)";
			$res=mysqli_query($connexio,$consultaPuntos);
			if(!$res){
				die ("No se pudo ejecutar la consulta SQL de puntos de valoración.");
			}
			while($f=mysqli_fetch_row($res)){
				echo $f[0];
				echo '★';
			}
		} else {
			echo "Hubo un error al actualizar tu favorito: " . mysqli_error($connexio);
		}
	}
}
?>