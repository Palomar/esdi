

<?php
include("header.php");
if (isset($_GET["id"])){
	if (intval($_GET["id"] == 0)){
		$id = 1;
	}else{
		$id = intval($_GET['id']);
	}

	if (is_int($id)){
		//Solo si es integer se procesará
		if(isset($_POST["coment"])){
			$coment = $_POST["coment"];
			$nombre = $_SESSION["user"];
			$hoy = date("Y-m-d H:i:s");
			$insert=sprintf("INSERT INTO comentarios_restaurantes( comentario, nombre, fecha, restaurantes_id ) VALUES ('%s','%s','%s','%d')",mysqli_real_escape_string($connexio,$coment),mysqli_real_escape_string($connexio,$nombre),mysqli_real_escape_string($connexio,$hoy),mysqli_real_escape_string($connexio,$id));
			$resultat=mysqli_query($connexio,$insert);
			if(!$resultat){
				die ("No se pudo ejecutar la consulta SQL");
			}
		}

		$consulta="SELECT * FROM restaurantes WHERE id = $id;";
		$resultat=mysqli_query ($connexio, $consulta);
		if (!$resultat) die ("ERROR: No se pudo ejecutar la consulta SQL");
		while($fila=mysqli_fetch_array ($resultat)){
		  $nombre = $fila['nombre'];
		  $url_img = $fila['url_img'];
		  $descripcion = $fila['descripcion'];
		  $url_map = $fila['url_map'];
		}


		echo '<div class="contentRestaurantes">
		<div class="content_background">
    <h1>RESTAURANTES</h1>

    <div class="fichaRest">
      <h3>'.$nombre.'</h3>
      <div class="img_txt_rest">
        <img data-aos="flip-right" src="'.$url_img.'" alt="restaurante udon"/>
        <div class="desc_gmaps">
          <p>'.$descripcion.'</p>
          <a href="//'.$url_map.'" class="menu-opc"><b>Google Maps</b></a>
        </div>
      </div>';
	  
	  echo '<div class="puntos" id="puntuacion">';
		
		if($id){
			$consultaPuntos = "SELECT round((SELECT sum(voto) FROM votos_restaurantes WHERE restaurantes_id = '".$id."') / (SELECT count(*) FROM votos_restaurantes WHERE restaurantes_id = '".$id."'),2)";
			$res=mysqli_query($connexio,$consultaPuntos);
			if(!$res){
				die ("No se pudo ejecutar la consulta SQL de puntos de valoración.");
			}
			while($f=mysqli_fetch_row($res)){
				if ($f[0] == null){
					echo '0';
				}else echo $f[0];
				echo '★';
			}
		}
		
		echo '</div>';
	  
	  if (isset($_SESSION["user"])){
      echo '<div  class="stars">
	   <div class="heartRecetas">
        <form action="">
          <input class="star star-5" id="star-5" type="radio" name="star" onClick="voto()" />
          <label class="star star-5" for="star-5"></label>
          <input class="star star-4" id="star-4" type="radio" name="star" onClick="voto()" />
          <label class="star star-4" for="star-4"></label>
          <input class="star star-3" id="star-3" type="radio" name="star" onClick="voto()" />
          <label class="star star-3" for="star-3"></label>
          <input class="star star-2" id="star-2" type="radio" name="star" onClick="voto()" />
          <label class="star star-2" for="star-2"></label>
          <input class="star star-1" id="star-1" type="radio" name="star" onClick="voto()" />
          <label class="star star-1" for="star-1"></label>
        </form>
		<div class="heartElement">';
		
		//SELECTOR FAVORITOS
			echo '<input id="toggle-heart" type="checkbox" onClick="fav()"/>
					<label for="toggle-heart">❤</label>';
			echo '</div>';
		
		
		//------------------>
      echo '</div></div>';
	  }
    echo '</div>
    <div class="coments_rest">
        <div style ="width:100%; margin-bottom:50px;">
          <h4>Comentarios</h4>';
		   if(isset($_SESSION["user"])){
			  echo '<form action="restaurantes.php?id='.$id.'" method="post">
				<textarea name="coment" id="comentario_rest" placeholder="Escribe un comentario..."></textarea>
				<button class="btncoment" type="submit">
			  </form>';
		   }
        echo '</div>
				</div>';


       $consulta="SELECT * FROM comentarios_restaurantes WHERE restaurantes_id =".$id;
       $resultat=mysqli_query ($connexio, $consulta);
       if (!$resultat) die ("ERROR: No se ha podido ejecutar la consulta SQL");
         while($fila=mysqli_fetch_array ($resultat))
         {
           $comentario = $fila['comentario'];
           $nick = $fila['nombre'];
        echo "   <div data-aos='fade-up-right' class='comentario'>";
        echo "     <div class='imagen_persona'>";
        echo "         <img src='images/vampirico.png' style='width:65px;height:65px;'' />";
        echo "     </div>";
        echo "     <div class='flecha'>";
        echo "     </div>";
        echo "     <div class='comentario_persona'>";
        echo "         <div class='datos_persona'>";
        echo "           <p><B>".$nick."</B></p>";
        echo "         </div>";
        echo "         <div class='comentario_texto'>";
        echo "           <p>".$comentario."</p>";
        echo "         </div>";
        echo "     </div>";
        echo "   </div>";
         }

       echo'</div>
		</div>';


	}else{
		echo "No hay dirección válida.";
		$id=0;
	}

}else{
	echo "No hay dirección válida.";
	$id=0;
}


?>

<!--SELECTOR FAVORITOS SCRIPT-->
		<script type="text/javascript">
		window.onload = onPageLoad();
		function onPageLoad() {
			var cor = document.getElementById("toggle-heart");
			var star1 = document.getElementById("star-1");
			var star2 = document.getElementById("star-2");
			var star3 = document.getElementById("star-3");
			var star4 = document.getElementById("star-4");
			var star5 = document.getElementById("star-5");
			
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			}
			xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                respuesta = xmlhttp.responseText.split(",");
				
				//CORAZÓN
				if (respuesta[0] == "true"){
					//Tiene que marcarlo
					cor.checked = true;
				}else{
					cor.checked = false;
				}
				//ESTRELLAS
				if (respuesta[1] != "false"){
					
					//Tiene que marcarlo
					switch(respuesta[1]) {
						case "1":
							star1.checked = true;
							break;
						case "2":
							star2.checked = true;
							break;
						case "3":
							star3.checked = true;
							break;
						case "4":
							star4.checked = true;
							break;
						case "5":
							star5.checked = true;
							break;
						default:
							star1.checked = false;
							star2.checked = false;
							star3.checked = false;
							star4.checked = false;
							star5.checked = false;
					} 
					
				}else{
					star1.checked = false;
					star2.checked = false;
					star3.checked = false;
					star4.checked = false;
					star5.checked = false;
				}
				
			}
			};
			xmlhttp.open("GET","resfav_backendLoad.php?id="+<?php echo $id; ?>,true);
			xmlhttp.send();
			
		}
		
		function fav() {
			var cor = document.getElementById("toggle-heart");
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			}
			xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                respuesta = xmlhttp.responseText.split(",");
			}
			};
			xmlhttp.open("GET","resfav_backend.php?id="+<?php echo $id; ?>,true);
			xmlhttp.send();
			
		}
		
		</script>
		<!--SELECTOR FAVORITOS SCRIPT TERMINA-->
		
		<!--SELECTOR ESTRELLAS SCRIPT-->
		<script type="text/javascript">
		function voto() {
			var star1 = document.getElementById("star-1");
			var star2 = document.getElementById("star-2");
			var star3 = document.getElementById("star-3");
			var star4 = document.getElementById("star-4");
			var star5 = document.getElementById("star-5");
			
			if(star1.checked == true){
				var voto = 1;
			}else if(star2.checked == true){
				var voto = 2;
			}else if(star3.checked == true){
				var voto = 3;
			}else if(star4.checked == true){
				var voto = 4;
			}else if(star5.checked == true){
				var voto = 5;
			}else {
				var voto = 0;
			}
			
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			}
			xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                respuesta = xmlhttp.responseText.split(",");
				$("#puntuacion").text(respuesta[1]);
			}
			};
			xmlhttp.open("GET","resvoto_backend.php?id="+<?php echo $id; ?>+"&voto="+voto,true);
			xmlhttp.send();
			
		}
		
		</script>
		<!--SELECTOR FAVORITOS SCRIPT TERMINA-->

<?php include('modal.php'); ?>
<?php include("footer.php") ?>
