
<?php
include("header.php");
?>

<?php
if (isset($_GET["id"])){
	
/*if(isset($_POST["id"])){
    $id= $_POST["id"];
}*/

	if (intval($_GET["id"] == 0)){
			$id = 1;
	}else{
			$id = intval($_GET['id']);
	}
	//Solamente si es integer se procesa el comentario
	
	if (is_int($id)){
		if(isset($_POST["coment"])){
		  $coment = $_POST["coment"];
		  $nombre = $_SESSION["user"];
		  $hoy = date("Y-m-d H:i:s");
		  $insert=sprintf("INSERT INTO comentarios( comentario, nombre, fecha, recetas_id) VALUES ('%s','%s','%s','%d')",mysqli_real_escape_string($connexio,$coment),mysqli_real_escape_string($connexio,$nombre),mysqli_real_escape_string($connexio,$hoy),mysqli_real_escape_string($connexio,$id));
		  $resultat=mysqli_query($connexio,$insert);
		  if(!$resultat){
			  die ("No se pudo ejecutar la consulta SQL");
		  }
		}
	
	}

?>

<?php

$consulta="SELECT * FROM recetas WHERE id = $id;";
$resultat=mysqli_query ($connexio, $consulta);
if (!$resultat) die ("ERROR: No se pudo ejecutar la consulta SQL");
  while($fila=mysqli_fetch_array ($resultat))
  {
      $id_receta = $fila['id'];
      $titulo = $fila['nombre'];
      $url_img = $fila['url_img'];
      $url_video = $fila['url_video'];
      $des = $fila['descripcion'];
    }


$consulta_2="SELECT * FROM recetas_has_ingredientes AS ri JOIN ingredientes AS i ON ri.ingredientes_id = i.id WHERE recetas_id = '".$id_receta."'";

$resConsulta2=mysqli_query($connexio, $consulta_2);
if (!$resConsulta2) die ("ERROR: No se pudo ejecutar la consulta SQL");
  $i=0;
  while($fila=mysqli_fetch_array_esdi($resConsulta2))
  {
	$ing_id[$i] = $fila['i.id'];
    $ing_nombre[$i] = $fila['i.nombre']; // array con los nombres de ingredientes de receta
    $ing_cantidad[$i]= $fila['i.cantidad'];
	$ing_urlimg[$i] = $fila['i.url_img'];
	$i++;
  }

//Dejo comentada esta linea porque está mal hecho el pasar los valores en IN así, lo dejo con la solución que hice yo
/*$consulta_3="SELECT
FROM ingredientes_categoria
WHERE ingrediente IN ("."'".$ing_nombre[0]."'".","."'".$ing_nombre[1]."'".","."'".$ing_nombre[2]."'".","."'".$ing_nombre[3]."'".")";*/
if (isset($ing_id)){
$ing_idPreparada = implode(',', $ing_id);

$consulta_3="SELECT * FROM ingredientes_has_categoria_ingredientes AS ic
JOIN categoria_ingredientes AS c ON ic.categoria_ingredientes_id = c.id
WHERE ingredientes_id IN(".$ing_idPreparada.")";

$resConsulta3=mysqli_query ($connexio, $consulta_3);
if (!$resConsulta3) die ("ERROR: No se pudo ejecutar la consulta SQL");
  $i=0;
  while($fila=mysqli_fetch_array_esdi($resConsulta3))
  {
    $cat_nom[$i] = $fila['c.nombre']; // array con los nombres de categoría y su url de imagen
    $cat_img[$i] = $fila['c.url_img'];
	$i++;
  }
}



?>


  <?php
  $alergia ="";
  $alergia_detectada=False;

  if(isset($SESSION['user'])){
    $consultaAlergia="SELECT * FROM usuario AS u
    JOIN usuarios_has_alergias AS ua ON u.id = ua.usuarios_id
    WHERE nombre = '".$_SESSION['user']."'"; //$user = $_SESSION['user']
    $resAlergia=mysqli_query ($connexio, $consultaAlergia);
    if (!$resAlergia) die ("ERROR: No se pudo ejecutar la consulta SQL");
		$i=0;
		while($fila=mysqli_fetch_array_esdi ($resAlergia))
		{
			$id_alergia[$i] = $fila['ua.alergias_id'];
			$i++;
		}

    }

      if(isset($SESSION['user'])){
	    $id_alergiaPreparada = implode(',',$id_alergia);  //Se necesita una array de alergias y no una única
      $consultaAlergia2="SELECT * FROM alergias WHERE id IN (".$id_alergiaPreparada.")";
      $resAlergia2=mysqli_query ($connexio, $consultaAlergia2);
      if (!$resAlergia2) die ("ERROR: No se pudo ejecutar la consulta SQL");
        $i = 0;
		while($fila=mysqli_fetch_array ($resultat))
        {
			$alergia[$i] = $fila['nombre'];
			$i++;
		}

      }

  ?>

<div class="recetas_2">
  <div class="titulo_receta_2">
      <h1 >RECETAS</h1>
  </div>
  <div class="pack_receta_2">
      <div class ="nombre_receta fadeInLeft">
          <h2 class="nombre_receta_titulo animated fadeInLeft">
              <?php echo $titulo;?>
          </h2>
      </div>
        <div  class="foto_ingredientes">
          <div data-aos="flip-right" class="imagen_receta">
          <img src="<?php echo $url_img?>" class="foto_receta_2" />
          </div>
          <div class="lista_receta">
            <ul>
              <li><?php if(isset($ing_cantidad)) echo $ing_cantidad[0]; ?>
                <div class="tooltip"><?php if(isset($ing_cantidad)) if($alergia == $ing_nombre[0]){
                  $alergia_detectada = True;
                  if(isset($ing_cantidad)){
					echo $ing_nombre[0];
				  }
                }else{
					if(isset($ing_cantidad)) echo "<b>".$ing_nombre[0]."</b>";
                }?>
                  <span class="tooltiptext">
                    <div class="contenedor_tooltip">
                      <div class="icono_cabezera_tooltip">
                          <img src="images/icono_arroz.png" style="width:65px;height:65px;"/>
                          <?php
                          if($alergia_detectada == True){
                              echo "<p class='alerg' style='color:red'>HEY FORASTERO!<br>Este producto esta marcado como alérgico!</p>";
                          }
                          ?>
                      </div>
                      <div class="componentes_tooltip">
                        <!--componente 1-->
                        <div class="componente_tooltip">
                          <div class="imagen_componente">
                            <img src="<?php echo $cat_img[0]; ?>" style="width:35px;height:35px;"/>
                          </div>
                          <div class="texto_componente">
                            <p><?php echo $cat_nom[0];?></p>
                          </div>
                        </div>
                        <!--componente 2-->
                        <div class="componente_tooltip">
                          <div class="imagen_componente">
                            <img src="<?php echo $cat_img[1]; ?>" style="width:35px;height:35px;"/>
                          </div>
                          <div class="texto_componente">
                            <p><?php echo $cat_nom[1];?></p>
                          </div>
                        </div>
                        <!--componente 3-->
                        <div class="componente_tooltip">
                          <div class="imagen_componente">
                            <img src="<?php echo $cat_img[2]; ?>" style="width:35px;height:35px;"/>
                          </div>
                          <div class="texto_componente">
                            <p><?php echo $cat_nom[2];?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </span>
                </div>
              </li>
              <?php
			  if(isset($ing_cantidad)){
                for($i =1;$i< count($ing_nombre);$i++){
                  echo "<li>".$ing_cantidad[$i]." <B>". $ing_nombre[$i]."</B>";
			
                  echo"</li>";
                }
			  }
              ?>
            </ul>
          </div>
        </div>
        <div class="descripcion_receta">
          <p class="p_receta"><?php echo $des;?></p>
        </div>
      <div data-aos="fade-up"
      data-aos-duration="1000" class="video">
        <iframe class="video_receta_2" src="<?php echo $url_video ?>"></iframe>
      </div>
      
	  
		<div class="puntos" id="puntuacion">
		<?php
		if($id){
			$consultaPuntos = "SELECT round((SELECT sum(voto) FROM votos_recetas WHERE recetas_id = '".$id."') / (SELECT count(*) FROM votos_recetas WHERE recetas_id = '".$id."'),2)";
			$res=mysqli_query($connexio,$consultaPuntos);
			if(!$res){
				die ("No se pudo ejecutar la consulta SQL de puntos de valoración.");
			}
			while($f=mysqli_fetch_row($res)){
				if ($f[0] == null){
					echo '0';
				}else echo $f[0];
				echo '★';
			}
		}
		?>
		</div>
	  
	  <?php
	  //VALORACIONES DE ESTRELLAS
      if (isset($_SESSION["user"])){
		
	  echo '<div class="stars">
        <div class="heartRecetas">
		<form action="">
          <input class="star star-5" id="star-5" type="radio" name="star" onClick="voto()" />
          <label class="star star-5" for="star-5"></label>
          <input class="star star-4" id="star-4" type="radio" name="star" onClick="voto()" />
          <label class="star star-4" for="star-4"></label>
          <input class="star star-3" id="star-3" type="radio" name="star" onClick="voto()" />
          <label class="star star-3" for="star-3"></label>
          <input class="star star-2" id="star-2" type="radio" name="star" onClick="voto()" />
          <label class="star star-2" for="star-2"></label>
          <input class="star star-1" id="star-1" type="radio" name="star" onClick="voto()" />
          <label class="star star-1" for="star-1"></label>
        </form>
		<div class="heartElement">';
		
		//SELECTOR FAVORITOS
			echo '<input id="toggle-heart" type="checkbox" onClick="fav()"/>
					<label for="toggle-heart">❤</label>';
			echo '</div>';
		
		
		//------------------>
		echo '</div></div>';
	    }
		?>
	  
        <?php
		echo'<div  class="coments_rest">
        <div style ="width:100%; margin-bottom:50px;">
          <h4>Comentarios</h4>';
          if(isset($_SESSION["user"])){

          echo'  <form action="recetas.php?id='.$id.'" method="post">
              <textarea name="coment" id="comentario" placeholder="Escribe un comentario..."></textarea>
              <button class="btncoment" type="submit">
            </form>';
          }

      echo '</div>';
       $consulta="SELECT * FROM comentarios WHERE recetas_id = $id;";
       $resultat=mysqli_query ($connexio, $consulta);
       if (!$resultat) die ("ERROR: No se pudo ejecutar la consulta SQL");
        while($fila=mysqli_fetch_array ($resultat))
        {
           $comentario = $fila['comentario'];
           $nick = $fila['nombre'];
        echo "   <div data-aos='fade-up-right' class='comentario'>";
        echo "     <div class='imagen_persona'>";
        echo "         <img src='images/vampirico.png' style='width:65px;height:65px;' />";
        echo "     </div>";
        echo "     <div class='flecha'>";
        echo "     </div>";
        echo "     <div class='comentario_persona'>";
        echo "         <div class='datos_persona'>";
        echo "           <p><B>".$nick."</B></p>";
        echo "         </div>";
        echo "         <div class='comentario_texto'>";
        echo "           <p>".$comentario."</p>";
        echo "         </div>";
        echo "     </div>";
        echo "   </div>";


		}

		echo '</div>';

}else{
	echo "No hay dirección válida.";
	$id=0;
}
?>

	</div>
</div>

      


<!--SELECTOR FAVORITOS SCRIPT-->
		<script type="text/javascript">
		window.onload = onPageLoad();
		function onPageLoad() {
			var cor = document.getElementById("toggle-heart");
			var star1 = document.getElementById("star-1");
			var star2 = document.getElementById("star-2");
			var star3 = document.getElementById("star-3");
			var star4 = document.getElementById("star-4");
			var star5 = document.getElementById("star-5");
			
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			}
			xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                respuesta = xmlhttp.responseText.split(",");
				
				//CORAZÓN
				if (respuesta[0] == "true"){
					//Tiene que marcarlo
					cor.checked = true;
				}else{
					cor.checked = false;
				}
				//ESTRELLAS
				if (respuesta[1] != "false"){
					
					//Tiene que marcarlo
					switch(respuesta[1]) {
						case "1":
							star1.checked = true;
							break;
						case "2":
							star2.checked = true;
							break;
						case "3":
							star3.checked = true;
							break;
						case "4":
							star4.checked = true;
							break;
						case "5":
							star5.checked = true;
							break;
						default:
							star1.checked = false;
							star2.checked = false;
							star3.checked = false;
							star4.checked = false;
							star5.checked = false;
					} 
					
				}else{
					star1.checked = false;
					star2.checked = false;
					star3.checked = false;
					star4.checked = false;
					star5.checked = false;
				}
				
			}
			};
			xmlhttp.open("GET","recfav_backendLoad.php?id="+<?php echo $id; ?>,true);
			xmlhttp.send();
			
		}
		
		function fav() {
			var cor = document.getElementById("toggle-heart");
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			}
			xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                respuesta = xmlhttp.responseText.split(",");
			}
			};
			xmlhttp.open("GET","recfav_backend.php?id="+<?php echo $id; ?>,true);
			xmlhttp.send();
			
		}
		
		</script>
		<!--SELECTOR FAVORITOS SCRIPT TERMINA-->
		
		<!--SELECTOR ESTRELLAS SCRIPT-->
		<script type="text/javascript">
		function voto() {
			var star1 = document.getElementById("star-1");
			var star2 = document.getElementById("star-2");
			var star3 = document.getElementById("star-3");
			var star4 = document.getElementById("star-4");
			var star5 = document.getElementById("star-5");
			
			if(star1.checked == true){
				var voto = 1;
			}else if(star2.checked == true){
				var voto = 2;
			}else if(star3.checked == true){
				var voto = 3;
			}else if(star4.checked == true){
				var voto = 4;
			}else if(star5.checked == true){
				var voto = 5;
			}else {
				var voto = 0;
			}
			
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			}
			xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                respuesta = xmlhttp.responseText.split(",");
				$("#puntuacion").text(respuesta[1]);
			}
			};
			xmlhttp.open("GET","recvoto_backend.php?id="+<?php echo $id; ?>+"&voto="+voto,true);
			xmlhttp.send();
			
		}
		
		</script>
		<!--SELECTOR FAVORITOS SCRIPT TERMINA-->
	  
<?php include('modal.php'); ?>

<?php include("footer.php") ?>
