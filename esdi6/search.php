<script type="text/javascript">
$(document).ready(function(){
	$('.expSearchFrom input[type="text"]').on("keyup input", function(){
		//Recoge la string introducida en el campo
		var term = $(this).val();
		var rs = $(this).siblings(".result");
		if(term.length){
			$.get("utilidades/search-backend.php", {query: term}).done(function(data){
				//Coloca las búsquedas
				rs.html(data);
			});
		} else{
			rs.empty();
		}
	});

	//Coloca la opción seleccionada
	$(document).on("click", ".result div p", function(){
		$(this).parents(".expSearchFrom").find('input[type="text"]').val($(this).text());
		$(this).parent(".result").empty();
		$(this).parent(".result div").remove();
		//Si está el focus fuera pero tiene texto, debe dejarlo visible
		document.getElementById("field").focus();
	});
	
	$(document).on("click","", function(){
		var rs = $(".expSearchFrom").siblings(".result");
		rs.empty();
		$(".expSearchFrom").find(".result").empty();
		$(".expSearchFrom").find(".result div").remove();
		
	});
	

});

</script>
	<!--<div class="search-box">
	    <input type="text" autocomplete="off" placeholder="Buscar..." name="search"/>
		<div class="result"></div>
	</div>-->
	<div class="expSearchBox">
		<div class="expSearchFrom">
			<input id="field" type="text" autocomplete="off" placeholder="Buscar..." name="search"/>    
			<div class="close">
				<span class="front"></span>
				<span class="back"></span>
			</div>
			<div class="result"></div>			
        </div>
    </div>
	